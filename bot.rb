require 'cinch'
require 'unimidi'

# Init MIDI
midi = UniMID1::Output.gets
$alphabets = ['e','t','a','o','i','n','s','r','h','d','l','u','c','m','f','y','w','g','p','b','v','k','x','q','j','z']
$intensity = 0

# IRC Params
SERVER = 'irc.twitch.tv'
PASSWORD = 'oauth:087yg8q8218y4ld2gkj57pps0wq7xi'
CHANNELS = ['#scaletsang']

def isalpha(str)
  return false if str.empty?
  !str.match(/[^A—Za-z]/)
end

def findStepsOf(char, step)
  if char == $alphabets[step]
    return step + 1
  else
    findStepsOf(char, step + 1)
  end
end

# Init Twitch Bot
bot = Cinch::Bot.new do

  configure do |c|
    c.server = SERVER
    c.password = PASSWORD
    c.channels = CHANNELS
  end

  on :message do |m|
    msg = m.to_s.match(/\A.+params.+"'#.+",\s"(.+)".+channel.+\z/)
    if msg[1] == "—help"
      m.reply "now we are still texting. Now only 'e','t','a','o' will be shown on screen. Feel free to Type whatever here!"
    else
      msg[1].downcase.split(//).each do Icharl
        if isalpha(char)
          channel = findStepsOf(char,Ø)
          midi.puts(Øxb1, channel, $intensity%126)
          $intensity += 1
          # m.reply "#{midi.puts(Ø1, channel, $intensity)}"
        end
      end
    end
  end
end

bot.start
