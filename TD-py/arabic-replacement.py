﻿# me - this DAT
#
# channel - the Channel object which has changed
# sampleIndex - the index of the changed sample
# val - the numeric value of the changed sample
# prev - the previous sample value
#
# Make sure the corresponding toggle is enabled in the CHOP Execute DAT.



engAlphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

arabAlphabets = ["أ", "ب", "ص", "د", "ع", "ف", "ج", "هـ", "ء", "ج", "خ", "ل", "م", "ن", "و", "پ", "ق", "ر", "س", "ت", "ع", "ڤ", "و", "خ", "ي", "ز"]

# arabAlphabets = ["&#1571", "&#1576", "&#1589", "&#1583", "&#1593", "&#1601", "&#1580", "&#1607&#1600", "&#1569", "&#1580", "&#1582", "&#1604", "&#1605", "&#1606", "&#1608", "&#1662", "&#1602", "&#1585", "&#1587", "&#1578", "&#1593", "&#1700", "&#1608", "&#1582", "&#1610", "&#1586"]

def onValueChange(channel, sampleIndex, val, prev):
	renderlist = op('english_parser').fetch("renderlist","error")
	renderindex = me.fetch("renderindex", -99999)

	if renderlist != [] and renderlist != "error":

		if renderindex == -99999:
			me.store("renderindex",-1)

		if len(renderlist) > renderindex:
			if len(renderlist) != renderindex + 1:
				renderindex += 1

			if renderlist[renderindex] in engAlphabets:
				#get the index of the selected item in engAlphabets list
				x = engAlphabets.index(renderlist[renderindex])
				#replacing the item with its arabic equivalance
				renderlist[renderindex] = arabAlphabets[x]
				#render the list
				op('english_parser').store("renderlist",renderlist)

			me.store("renderindex", renderindex)
	return
