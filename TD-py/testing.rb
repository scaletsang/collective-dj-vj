hash = Hash[('a'..'z').zip(1..26)]

m = "h  ikjhs s"

def isalpha(str)
  return false if str.empty?
  !str.match(/[^A-Za-z]/)
end

m.split('').each do |char|
  if isalpha(char)
    puts hash[char]
    end
end

#<Cinch::Message @raw=":scaletsang!scaletsang@scaletsang.tmi.twitch.tv PRIVMSG #scaletsang :hi" @params=["#scaletsang", "hi"] channel=#<Channel name="#scaletsang"> user=#<Bot nick="scaletsang">>
