# me - this DAT
#
# frame - the current frame
# state - True if the timeline is paused
#
# Make sure the corresponding toggle is enabled in the Execute DAT.

def onStart():
	op('english_parser').unstore("queue")
	op('english_parser').unstore("renderlist")
	op('english_parser').store("queue",[])
	op('english_parser').store("renderlist",[])
	return


def onFrameStart(frame):
	#get the renderlist
	renderlist = op('english_parser').fetch("renderlist", "error")

	op('rendered_text_display').par.text= "".join(renderlist) 

	return
