# me - this DAT
#
# channel - the Channel object which has changed
# sampleIndex - the index of the changed sample
# val - the numeric value of the changed sample
# prev - the previous sample value
#
# Make sure the corresponding toggle is enabled in the CHOP Execute DAT.


def onValueChange(channel, sampleIndex, val, prev):

	#store one alphebet to the storage at each tick of the fast_clock,
	#the storage will be rendered every frame with the excute DAT
	if channel.name == 'fast_clock':
		queue = me.fetch("queue", "error")
		renderlist = me.fetch("renderlist", "error")
		#Debugging
		# print("hi",me.fetch("renderlist", "error"))
		# print("hi",me.fetch("queue", "error"))

		if queue == "error":
			me.store("queue", [])
		if queue != []:
			if renderlist == "error":
				me.store("renderlist", [])
			else:
				if len(renderlist) != 0 and (len(renderlist) % 336) == 0:
					#storage control, delete 168 items if the storage exceeded 336 items
					renderlist[168:]
					#updating the render index because we have shortened the render list
					renderindex = op('arabic_parser').fetch("renderindex", -99999)
					if renderindex <= 168:
						op('arabic_parser').store("renderindex", -1)
					else: op('arabic_parser').store("renderindex", (renderindex % 168) -1)

				if len(renderlist) != 0 and (len(renderlist) % 28) == 0: renderlist.append("\n")
					#add newline at every 28 items in the storage

				renderlist.append(queue.pop(0))
				me.store("queue", queue)
				me.store("renderlist", renderlist)
	return
