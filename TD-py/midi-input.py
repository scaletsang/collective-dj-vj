# me - this DAT
#
# dat - the DAT that received the event
# rowIndex - the row number that was added
# message - a readable description of the event
# channel - the numeric value of the event channel
# index - the numeric value of the event index
# value - the numeric value of the event value
# input - true when the event was received
# bytes - a byte array of the received event
#
# Example:
# message  channel index value     bytes
# Note On  1        63   127       90 2f 127

def onReceiveMIDI(dat, rowIndex, message, channel, index, value, input, bytes):

	alphabets = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

	queue = op('english_parser').fetch("queue", "error")
	if queue != "error":
		queue.append(alphabets[index-1])
		soundtrigger(alphabets[index-1])
		op('english_parser').store("queue", queue)

	return


def soundtrigger(input):
	if input == 'c':
		if op('getCurrentScene').chan('currentscene') == 0:
			op('abletonSong').FireScene(1)
		elif op('getCurrentScene').chan('currentscene') == 1:
			op('abletonSong').FireScene(0)
	return
